CC ?= cc
CFLAGS ?= -O2 -ggdb -Wall -Wextra
PKG_CONFIG ?= pkg-config

CFLAGS_openrc = `$(PKG_CONFIG) --cflags openrc`
LIBS_openrc = `$(PKG_CONFIG) --libs openrc`

all: rc-status-page

rc-status-page: rc-status-page.c
	$(CC) -std=c11 $(CFLAGS) $(CFLAGS_openrc) -o $@ $< $(LDFLAGS) $(LIBS_openrc)

.PHONY: clean
clean:
	rm rc-status-page
